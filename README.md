# Concord app

The Concord Desktop App.

Please check the [this repository wiki](https://gitlab.com/concordgroup/ConcordApp/wikis/home) for more information.

New developers, please read [here](https://gitlab.com/concordgroup/ConcordApp/wikis/New-developers).